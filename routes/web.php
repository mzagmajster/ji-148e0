<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

use Illuminate\Session;
use Illuminate\Http\Request;

Route::get('/', function() {
	return View::make('index');
});

Route::group(array('prefix' => 'api'), function(){
	Route::group(array('prefix' => 'auth'), function() {
		Route::post('login', 'AuthController@login');
		Route::get('logout', 'AuthController@logout');
		Route::get('is-authenticated', 'AuthController@isLoggedIn');
	});

	Route::group(array('prefix' => 'users'), function(){
		Route::get('get/{uuid}', 'UsersController@getUser');
		Route::post('add', 'UsersController@add');
		Route::post('edit/{uuid}', 'UsersController@edit');
		Route::get('remove/{uuid}', 'UsersController@remove');
		Route::get('/', 'UsersController@show');
	});

	Route::group(array('prefix' => 'temp'), function(){
		Route::get('csrf', function(){
			return array('_token' => csrf_token());
		});

		Route::get('sess', function(Request $request){
			return response()->json($request->session()->all());
		});
	});
});
