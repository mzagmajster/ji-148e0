<?php

use Illuminate\Database\Seeder;
use Illuminate\Hashing\BcryptHasher;


class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$data = array(
			array('first_name' => 'Matic', 'last_name' => 'Zagmajster', 'birth_date' => '1801-07-06', 'email' => 'zagm101@gmail.com', 'password' => "test", 'created' => '2017-08-01 23:25:00', 'admin' => "true"),

			array("first_name"=>"Veronica","last_name"=>"Levy","birth_date"=>"1969-12-31","email"=>"Cum@Curabiturut.co.uk","password"=>"test","created"=>"2017-10-20 02:54:12","admin"=>"false"),
			array("first_name"=>"Andrew","last_name"=>"Simpson","birth_date"=>"1969-12-31","email"=>"penatibus.et@arcueuodio.ca","password"=>"test","created"=>"2017-11-05 10:58:48","admin"=>"false"),
			array("first_name"=>"Naida","last_name"=>"Norris","birth_date"=>"1969-12-31","email"=>"pede.Cum.sociis@amagnaLorem.org","password"=>"test","created"=>"2018-01-12 11:56:34","admin"=>"false"),
			array("first_name"=>"Reese","last_name"=>"Charles","birth_date"=>"1969-12-31","email"=>"Ut.nec.urna@eu.com","password"=>"test","created"=>"2018-03-29 15:17:08","admin"=>"false"),
			array("first_name"=>"Charissa","last_name"=>"Whitaker","birth_date"=>"1969-12-31","email"=>"vehicula@luctussit.com","password"=>"test","created"=>"2018-05-07 18:08:37","admin"=>"false"),
			array("first_name"=>"Kirestin","last_name"=>"Donovan","birth_date"=>"1969-12-31","email"=>"nec.luctus@acurna.co.uk","password"=>"test","created"=>"2018-04-23 08:59:18","admin"=>"false"),
			array("first_name"=>"Wendy","last_name"=>"Wooten","birth_date"=>"1969-12-31","email"=>"Duis@cubiliaCurae.com","password"=>"test","created"=>"2018-02-14 13:34:42","admin"=>"false"),
			array("first_name"=>"Roary","last_name"=>"Richmond","birth_date"=>"1969-12-31","email"=>"odio.sagittis.semper@odioAliquam.ca","password"=>"test","created"=>"2017-12-16 00:17:33","admin"=>"false"),
			array("first_name"=>"Hakeem","last_name"=>"Bridges","birth_date"=>"1969-12-31","email"=>"purus@ametornare.com","password"=>"test","created"=>"2018-09-10 15:14:00","admin"=>"false"),
			array("first_name"=>"Aquila","last_name"=>"Mccarty","birth_date"=>"1969-12-31","email"=>"metus.In@necleo.ca","password"=>"test","created"=>"2018-08-10 01:20:13","admin"=>"false"),
			array("first_name"=>"Ryan","last_name"=>"Dickson","birth_date"=>"1969-12-31","email"=>"consectetuer@Maurisvestibulum.co.uk","password"=>"test","created"=>"2018-08-22 20:11:04","admin"=>"false"),
			array("first_name"=>"Harper","last_name"=>"Franco","birth_date"=>"1969-12-31","email"=>"rhoncus.Donec.est@loremac.edu","password"=>"test","created"=>"2018-01-03 18:40:42","admin"=>"false"),
			array("first_name"=>"Ahmed","last_name"=>"Duncan","birth_date"=>"1969-12-31","email"=>"lorem@eueuismod.ca","password"=>"test","created"=>"2018-03-12 22:04:48","admin"=>"false"),
			array("first_name"=>"Guinevere","last_name"=>"Brady","birth_date"=>"1969-12-31","email"=>"lectus.Cum.sociis@non.com","password"=>"test","created"=>"2017-09-13 21:31:22","admin"=>"false"),
			array("first_name"=>"Lev","last_name"=>"Roberson","birth_date"=>"1969-12-31","email"=>"Curabitur@Class.ca","password"=>"test","created"=>"2018-09-17 08:15:59","admin"=>"false"),
			array("first_name"=>"Hu","last_name"=>"Byers","birth_date"=>"1969-12-31","email"=>"Nulla.eget.metus@temporest.net","password"=>"test","created"=>"2018-07-31 15:59:47","admin"=>"false"),
			array("first_name"=>"Adele","last_name"=>"Scott","birth_date"=>"1969-12-31","email"=>"quam.Pellentesque.habitant@feugiatmetussit.org","password"=>"test","created"=>"2018-04-27 09:52:20","admin"=>"false"),
			array("first_name"=>"Lavinia","last_name"=>"Kidd","birth_date"=>"1969-12-31","email"=>"at.libero.Morbi@vestibulummassa.edu","password"=>"test","created"=>"2018-08-16 07:23:26","admin"=>"false"),
			array("first_name"=>"Ferris","last_name"=>"Cantrell","birth_date"=>"1969-12-31","email"=>"sollicitudin.adipiscing@egestas.edu","password"=>"test","created"=>"2018-02-15 14:49:09","admin"=>"false"),
			array("first_name"=>"Armando","last_name"=>"Robles","birth_date"=>"1969-12-31","email"=>"purus.accumsan@tristique.ca","password"=>"test","created"=>"2017-12-12 15:42:27","admin"=>"false"),
			array("first_name"=>"Hiram","last_name"=>"Leon","birth_date"=>"1969-12-31","email"=>"adipiscing.elit.Etiam@utsemNulla.org","password"=>"test","created"=>"2018-03-08 18:16:11","admin"=>"false"),
			array("first_name"=>"Kieran","last_name"=>"Flowers","birth_date"=>"1969-12-31","email"=>"diam.luctus@nonbibendum.net","password"=>"test","created"=>"2018-02-19 09:31:58","admin"=>"false"),
			array("first_name"=>"Heather","last_name"=>"Little","birth_date"=>"1969-12-31","email"=>"mauris.a@egestasa.org","password"=>"test","created"=>"2017-10-22 15:09:28","admin"=>"false"),
			array("first_name"=>"Ashely","last_name"=>"Davis","birth_date"=>"1969-12-31","email"=>"tristique@milorem.edu","password"=>"test","created"=>"2018-02-17 08:09:31","admin"=>"false"),
			array("first_name"=>"Cole","last_name"=>"Hester","birth_date"=>"1969-12-31","email"=>"urna.justo.faucibus@egestasAliquamfringilla.net","password"=>"test","created"=>"2018-07-04 07:18:59","admin"=>"false"),
			array("first_name"=>"Maya","last_name"=>"Burch","birth_date"=>"1969-12-31","email"=>"dui.Cras.pellentesque@sedest.ca","password"=>"test","created"=>"2018-05-16 11:44:50","admin"=>"false"),
			array("first_name"=>"Ima","last_name"=>"Brown","birth_date"=>"1969-12-31","email"=>"nibh.sit@interdumligula.ca","password"=>"test","created"=>"2018-07-06 17:46:51","admin"=>"false"),
			array("first_name"=>"Desiree","last_name"=>"Avila","birth_date"=>"1969-12-31","email"=>"Cras.eget.nisi@orcilacusvestibulum.com","password"=>"test","created"=>"2018-07-12 10:44:39","admin"=>"false"),
			array("first_name"=>"Amery","last_name"=>"Vang","birth_date"=>"1969-12-31","email"=>"nibh@Nullaeu.org","password"=>"test","created"=>"2018-07-15 12:12:48","admin"=>"false"),
			array("first_name"=>"Chancellor","last_name"=>"Hinton","birth_date"=>"1969-12-31","email"=>"In@ut.net","password"=>"test","created"=>"2017-10-15 14:34:39","admin"=>"false"),
			array("first_name"=>"Idola","last_name"=>"Maynard","birth_date"=>"1969-12-31","email"=>"et@Praesenteudui.net","password"=>"test","created"=>"2018-03-26 22:02:34","admin"=>"false"),
			array("first_name"=>"Gwendolyn","last_name"=>"Everett","birth_date"=>"1969-12-31","email"=>"luctus@congueaaliquet.edu","password"=>"test","created"=>"2017-08-19 12:12:59","admin"=>"false"),
			array("first_name"=>"Portia","last_name"=>"Meadows","birth_date"=>"1969-12-31","email"=>"risus@ornarefacilisis.ca","password"=>"test","created"=>"2018-07-04 17:17:14","admin"=>"false"),
			array("first_name"=>"Elvis","last_name"=>"Love","birth_date"=>"1969-12-31","email"=>"placerat.orci@Aliquam.com","password"=>"test","created"=>"2018-09-05 09:35:47","admin"=>"false"),
			array("first_name"=>"Allen","last_name"=>"Hickman","birth_date"=>"1969-12-31","email"=>"molestie.orci.tincidunt@anequeNullam.edu","password"=>"test","created"=>"2017-10-30 09:30:30","admin"=>"false"),
			array("first_name"=>"Malcolm","last_name"=>"Savage","birth_date"=>"1969-12-31","email"=>"gravida@eu.ca","password"=>"test","created"=>"2017-10-12 12:41:18","admin"=>"false"),
			array("first_name"=>"Chelsea","last_name"=>"Weber","birth_date"=>"1969-12-31","email"=>"eget@et.edu","password"=>"test","created"=>"2018-07-23 02:38:54","admin"=>"false"),
			array("first_name"=>"Martha","last_name"=>"Tucker","birth_date"=>"1969-12-31","email"=>"Aliquam@Inornare.edu","password"=>"test","created"=>"2017-08-10 20:26:49","admin"=>"false"),
			array("first_name"=>"Abdul","last_name"=>"Pittman","birth_date"=>"1969-12-31","email"=>"et.magnis.dis@lectus.net","password"=>"test","created"=>"2018-09-09 19:45:15","admin"=>"false"),
			array("first_name"=>"Shaine","last_name"=>"Irwin","birth_date"=>"1969-12-31","email"=>"placerat.augue.Sed@Suspendissesed.edu","password"=>"test","created"=>"2018-08-13 17:25:06","admin"=>"false"),
			array("first_name"=>"Brandon","last_name"=>"Guzman","birth_date"=>"1969-12-31","email"=>"augue@non.edu","password"=>"test","created"=>"2018-09-06 15:05:54","admin"=>"false"),
			array("first_name"=>"Uriel","last_name"=>"Nguyen","birth_date"=>"1969-12-31","email"=>"Aliquam.rutrum@Sed.com","password"=>"test","created"=>"2018-07-02 04:19:53","admin"=>"false"),
			array("first_name"=>"Silas","last_name"=>"Ramos","birth_date"=>"1969-12-31","email"=>"nibh@tincidunt.org","password"=>"test","created"=>"2018-04-04 17:57:43","admin"=>"false"),
			array("first_name"=>"Tashya","last_name"=>"Jenkins","birth_date"=>"1969-12-31","email"=>"dui.nec@vulputateposuerevulputate.net","password"=>"test","created"=>"2017-09-01 23:01:24","admin"=>"false"),
			array("first_name"=>"Helen","last_name"=>"Bauer","birth_date"=>"1969-12-31","email"=>"montes@fermentum.org","password"=>"test","created"=>"2018-06-07 03:23:59","admin"=>"false"),
			array("first_name"=>"Henry","last_name"=>"Blair","birth_date"=>"1969-12-31","email"=>"sagittis@metusVivamuseuismod.ca","password"=>"test","created"=>"2017-10-24 07:06:25","admin"=>"false"),
			array("first_name"=>"Noelle","last_name"=>"Mullen","birth_date"=>"1969-12-31","email"=>"a@elitafeugiat.net","password"=>"test","created"=>"2018-01-03 07:12:14","admin"=>"false"),
			array("first_name"=>"Alvin","last_name"=>"Mueller","birth_date"=>"1969-12-31","email"=>"mi@sitamet.co.uk","password"=>"test","created"=>"2017-11-13 12:15:29","admin"=>"false"),
			array("first_name"=>"Vernon","last_name"=>"Joyce","birth_date"=>"1969-12-31","email"=>"In.ornare.sagittis@aliquamiaculislacus.ca","password"=>"test","created"=>"2018-09-04 17:56:46","admin"=>"false"),
			array("first_name"=>"Martha","last_name"=>"Bernard","birth_date"=>"1969-12-31","email"=>"Curabitur@bibendumullamcorper.com","password"=>"test","created"=>"2018-05-15 22:07:30","admin"=>"false")
		);

		$o = new BcryptHasher();
		foreach ($data as $user) {
			$user['password'] = $o->make($user['password']);
			$user['admin'] = ($user['admin'] == "false") ? false : true;
			DB::table('users')->insert($user);
		}
	
	}
}
