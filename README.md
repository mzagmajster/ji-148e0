# User management - backend

This is backend part of simple user management system using Laravel 5.3 and Angular 4. Frontend can be found [here](https://gitlab.com/Zag/ji-148e0-frontend).

## Getting Started

Follow instructions below to prepare project for development.

### Prerequisites

To successfully use development version of this project you will need to install Laravel 5.3.

Please refer to [this](https://laravel.com/docs/5.3/installation) site for instructions on how to install Laravel on your system.

### Installing

Clone repository and move to project root.

Run development server.

```
php artisan serve
```

## Running the tests

Currently there is no automated tests for this project.

## Contributing

Please read ```CONTRIBUTING.md``` for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository

## Authors

Inital content for this project was provided by Matic Zagmajster. For more information please see ```AUTHORS``` file.

## License

This project is licensed under the MIT License - see the ```LICENSE.md``` file for details.

