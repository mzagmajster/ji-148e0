<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Facades\Storage;

use Validator;
use Log;
use Auth;
use App\User;
use App\MessageBag;


class UsersController extends Controller
{
	private $_messageBag;

	public function __construct() {
		$this->middleware('auth');
		$this->_messageBag = new MessageBag();
	}

	private function _getUserValidator(Request &$request, $for='member', $moreFields=array()) {
		$memberFields = array(
			'first_name'	=> 'required|max:255',
			'last_name'		=> 'required|max:255',
			'birth_date'	=> 'nullable|date_format:Y-m-d',
			'email' 		=> 'required|email',
			'password'		=> 'required|max:255',
			'password2'		=> 'same:password',
			'avatar'		=> 'nullable|image'
		);

		$adminFields = array(
			'admin' => 'required|boolean'
		);

		if($for == 'admin') {
			$memberFields = array_merge($memberFields, $adminFields);
		}

		// Give a chance of applying slightly different validators for specific cases.
		$memberFields = array_merge($memberFields, $moreFields);

		return Validator::make($request->all(), $memberFields);
	}

	private function _addValidationMessages($msgs) {
		foreach ($msgs as $m) {
			$this->_messageBag->addMessage($m, 'danger');
		}
	}

	private function _extractUserRequestData(&$request, $f) {
		$requestData = $request->only($f);
		$dbData = array();

		foreach ($f as $field) {
			switch ($field) {
				case 'birth_date':
					$dbData[$field] = (strlen($requestData[$field])) ? $requestData[$field] : NULL;
					break;
				case 'avatar';
					break;
				default:
					$dbData[$field] = $requestData[$field];
					break;
			}
		}
		return $dbData;
	}

	public function getUser($uuid) {
		$r = array('success' => false, 'messages' => &$this->_messageBag->messages, 'obj' => array('uuid' => -1));

		$u = User::findOrFail($uuid);
		$r['obj'] = $u->getUserFields();
		$r['success'] = true;

		return $r;
	}

	public function add(Request $request) {

		if(!Auth::user()->isAdmin()) {
			return abort(403);
		}

		$r = array('success' => false, 'messages' => &$this->_messageBag->messages);
		$validator = $this->_getUserValidator($request, 'admin');

		if($validator->fails()) {
			Log::info('Validation failed.');
			$this->_addValidationMessages($validator->messages()->all());
			return $r;
		}

		$eo = User::where('email', $request->email)->first();
		if($eo !== NULL) {
			$this->_messageBag->addMessage(sprintf("Email %s is already in use, please choose another email.", $request->email), 'danger');
			return $r;
		}

		// Basic attributes.
		$u = new User();
		$dbData = $this->_extractUserRequestData(
			$request, 
			$u->getFillableByStatus('admin')
		);
		$u->fill($dbData);

		// Password.
		$u->setPassword($request->password);

		// Auto fields.
		$u->created = \gmdate('Y-m-d H:i:s');
		
		// Avatar.
		if($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
			$p = $request->file('avatar')->store('avatars');
			$u->avatar = basename($p);
		}

		$u->save();

		$r['success'] = true;
		$this->_messageBag->addMessage(sprintf("User with email: %s was successfully created.", $u->email), 'success');
		
		return $r;
	}

	public function remove($uuid) {
		$r = array('success' => false, 'messages' => &$this->_messageBag->messages);

		// Admin only.
		if(!Auth::user()->isAdmin()) {
			return abort(403);
		}

		// Do not delete user if its in use.
		if($uuid == Auth::user()->uuid) {
			$this->_messageBag->addMessage(sprintf("Account with ID: %d is in use and cannot be deleted.", $uuid), 'danger');
			return $r;
		}

		$u = User::findOrFail($uuid);

		if($u->avatar !== NULL) {
			$p = 'avatars/' . $u->avatar;
			Storage::delete($p);
		}
		
		User::destroy($uuid);

		$this->_messageBag->addMessage(sprintf("Account with ID: %d was successfully deleted.", $uuid), 'success');
		$r['success'] = true;
		return $r;
	}

	public function edit(Request $request, $uuid) {
		$r = array('success' => false, 'messages' => &$this->_messageBag->messages);

		$u = User::findOrFail($uuid);

		// User can edit his account but it cannot edit accounts of other users.
		if(Auth::user()->uuid != $uuid && !Auth::user()->isAdmin()) {
			$this->_messageBag->addMessage("You are not allowed to edit this account because you are not admin.", 'danger');
			return $r;
		}

		$allowedViews = array('member', 'admin');
		$view = $request->input('view');
		Log::info('view ' . $view);
		if(!in_array($view, $allowedViews)) {
			$view = 'member';
		}

		// Add admin-only fields if admin is editing.
		$for = (Auth::user()->isAdmin() && $view == 'admin') ? 'admin' : 'member';
		$validator = $this->_getUserValidator($request, $for, array('password' => 'nullable|max:255'));

		if($validator->fails()) {
			$this->_addValidationMessages($validator->messages()->all());
			return $r;
		}

		// Save.
		$dbData = $this->_extractUserRequestData(
			$request,
			$u->getFillableByStatus($for)
		);
		$u->fill($dbData);

		// Handle password change.
		if(strlen($request->password)) {
			$u->setPassword($request->password);
		}

		// Avatar.
		if($request->hasFile('avatar')) {
			Log::info('Avatar submitted.');
			if($request->file('avatar')->isValid()) {
				$path = $request->file('avatar')->store('avatars');
				Log::info('Avatars uploaded: ' . $path . '.');
				$u->avatar = basename($path);

			}
			else {
				$this->_messageBag->addMessage("Failed to upload file.", 'danger');
			}
		}
		
		$u->save();

		// We have to update current user in Laravel system.
		if($u->uuid == Auth::user()->uuid) {
			Auth::setUser($u);
		}

		$this->_messageBag->addMessage(
			sprintf("%s %s's account was successfully updated.", $u->first_name, $u->last_name),
			'success'
		);
		$r['success'] = true;

		return $r;
	}

	public function show(Request $request) {
		$allowedOrderFields = array('uuid', 'first_name', 'last_name', 'created');
		$allowedSortValues = array('asc', 'desc');

		$orderBy = $request->input('order-by', 'uuid');
		$orderBy = (in_array($orderBy, $allowedOrderFields)) ? $orderBy : 'uuid';

		$orderDirection = $request->input('order-direction', 'asc');
		$orderDirection = (in_array($orderDirection, $allowedSortValues)) ? $orderDirection : 'asc';

		$skip = filter_var($request->input('skip', 0), FILTER_VALIDATE_INT);
		$skip = ($skip !== false) ? $skip : 0;

		$take = filter_var($request->input('take', 10), FILTER_VALIDATE_INT);
		$take = ($take !== false) ? $take : 10;

		$users = User::orderBy($orderBy, $orderDirection)
			->skip($skip)
			->take($take)
			->get();

		$r = array();
		foreach ($users as $user) {
			$r[] = $user->getUserFields(array('uuid', 'first_name', 'last_name', 'email', 'avatar', 'admin'));
		}
		return $r;
	}
}
