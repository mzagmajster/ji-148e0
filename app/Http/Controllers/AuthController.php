<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;

use Auth;
use Log;
use App\User;
use App\MessageBag;

class AuthController extends Controller
{
	private $_messageBag;

	public function __construct() {
		$this->_messageBag = new MessageBag();
	}

	public function login(Request $request) {
		$r = array(
			'success' => false,
			'messages' => &$this->_messageBag->messages,
			'obj' => array(
				'uuid' => -1
			)
		);

		$this->validate($request, [
			'email' => 'required|max:255',
			'password' => 'required'
		]);

		$u = User::where('email', $request->email)->first();
		if($u === NULL) {
			$this->_messageBag->addMessage(
				sprintf("User with email %s does not exist in database.", $request->email),
				'danger'
			);
			return $r;
		}

		$p_obj = new BcryptHasher();
		if($p_obj->check($request->password, $u->password)) {
			Log::info($u);
			Log::info(Auth::login($u));

			$r['success'] = true;

			// Populate obj array.
			$r['obj'] = $u->getUserFields();
			

			$this->_messageBag->addMessage(
				"Login succeeded.",
				'success'
			);
		}
		else {
			$this->_messageBag->addMessage(
				"Invalid passowrd.",
				'danger'
			);
		}

		return $r;
	}

	public function logout() {
		$r = array(
			'success' => true,
			'messages' => &$this->_messageBag->messages,
			'obj' => array(
				'uuid' => -1
			)
		);
		Auth::logout();

		if(Auth::check()) {
			$r['success'] = false;
			$r['obj']['uuid'] = Auth::user()->id;
			$this->_messageBag->addMessage(
				"Logout failed.",
				'danger'
			);
		}
		else {
			$this->_messageBag->addMessage(
				"Logout succedeed.",
				'success'
			);
		}

		return $r;
	}

	public function isLoggedIn() {
		$r = array('success' => Auth::check(), 'obj' => array('uuid' => -1));

		if($r['success']) {
			$r['obj'] = Auth::user()->getUserFields();
		}

		return $r;
	}
}
