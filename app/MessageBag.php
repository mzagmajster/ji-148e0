<?php

namespace App;

class MessageBag {
	public $messages;

	public function __construct() {
		$this->messages = array();
	}

	public function addMessage($message, $category) {
		$this->messages[] = array($message, $category);
	}

	public function getMessages() {
		$all = $this->messages;
		$this->_messages = array();
		return $all;
	}
}
