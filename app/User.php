<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name', 'last_name', 'birth_date', 'email', 'admin'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'avatar', 'password', 'created', 'remember_token'
	];

	protected $primaryKey = 'uuid';

	public $timestamps = false;

	public function getUserFields($rf=array()) {
		// Declare available fields.
		$af = array('uuid', 'first_name', 'last_name', 'birth_date', 'avatar', 'email', 'created', 'admin', 'status');
		if(!count($rf)) {
			$rf = $af;
		}

		$f = array();

		foreach ($rf as $field) {
			switch ($field) {
				case 'uuid':
					$f[$field] = $this->uuid;
					break;
				case 'first_name':
					$f[$field] = $this->first_name;
					break;
				case 'last_name':
					$f[$field] = $this->last_name;
					break;
				case 'birth_date':
					$f[$field] = $this->birth_date;
					break;
				case 'avatar':
					$f[$field] = (!$this->avatar) ? NULL : Storage::url('avatars/'. $this->avatar);
					break;
				case 'email':
					$f[$field] = $this->email;
				case 'created':
					$f[$field] = $this->created;
					break;
				case 'created':
					$f[$field] = $this->created;
					break;
				case 'admin':
					$f[$field] = $this->admin;
					break;
				// Derived fields.
				case 'status':
					$f[$field] = ($this->isAdmin()) ? "Administrator" : "Member";
					break;
			}
		}

		return $f;
	}

	public function setPassword($password) {
		$po = new BcryptHasher();
		$this->password = $po->make($password);
	}

	public function checkPassword($password) {
		$po = new BcryptHasher();
		return $po->check($password, $this->password);
	}

	public function isAdmin() {
		return ($this->admin) ? true : false;
	}

	public function getFillableByStatus($status) {
		$r = $this->getFillable();

		if($status == 'member') {
			unset($r[array_search('admin', $r)]);
		}
		
		return $r;
	}
}
